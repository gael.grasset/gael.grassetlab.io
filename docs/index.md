I have a real passion for smart things (all smart devices, smart home, everything smart and techy !)... 

So I recently got in action and started to watch a real number of youtube Channels, got back to my electronics (bought a few resistors, sensors, arduinos, etc...), and after a lot of trial and error I finally got the optimal setup for me. 

The description of this setup, and all useful links are documented [here](setup/overall-setup.md).

## My requirements

I wanted to...

* Control my home via Google Assistant (Google Homes, and my Pixel 2 XL)
* Control my home from everywhere, not just my home network
* Get a cheap but reliable and quick system (not like "Turn the lights off"... Wait 5s... "Lights finally turn off")
* Have fairly simple setup, easy to copy and to setup again from scratch
* Be able to backup and restore my data
* Collect data and get some (un)useful stats about my home
