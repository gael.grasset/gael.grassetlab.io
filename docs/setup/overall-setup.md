The basic infrastructure schema looks like this :

![Home Automation Setup](../images/Infra-HomeAutomation.png "Home automation setup")

## Main components

* [Openhab 2](https://www.openhab.org/) for the main hub (the brain of the smart home).
* [Myopenhab.org](https://myopenhab.org/) to access openhab 2 from everywhere.
* [Google Assistant](https://assistant.google.com) to control OpenHab 2
* Some Z-Wave switches, mainly
    * [HS-WS100](https://shop.homeseer.com/products/homeseer-hs-ws100-z-wave-plus-scene-capable-wall-switch) Switches
    * [HS-WD100](https://shop.homeseer.com/products/best-z-wave-dimmer) Dimmers
* An [Aeotec Z-Stick Gen5](https://aeotec.com/z-wave-usb-stick) to access z-wave networks from my Openhab server
* A DSC Alarm system, monitored, and connected to my network with a [Envisalink 4 card](https://www.aartech.ca/envisalink-evl4.html)
* Some ESP8266 sensors, home-made.