<!-- ![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg) -->

---

My Gitlab pages generated with [mkDocs]

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. [Install](https://www.mkdocs.org/#installation) mkDocs
1. Test installation: `mkdocs serve` and open your browser at [http://localhost:8000](http://localhost:8000)
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at mkDocs's [documentation][https://www.mkdocs.org/].

