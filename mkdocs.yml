# Project information
site_name: 'My smart devices'
site_description: 'Documentation of my setup and personal projects'
site_author: 'Gael Grasset'
site_url: 'https://www.smart-devices.ca/'

# Repository
repo_name: 'gael.grasset/gael.grasset.gitlab.io'
repo_url: 'https://gitlab.com/gael.grasset/gael.grasset.gitlab.io'

# Copyright
copyright: 'Copyright &copy; 2018 - 2020 Gael Grasset'

# Configuration
theme:
  name: 'material'
  language: 'en'
  palette:
    primary: 'indigo'
    accent: 'indigo'
  logo:
    icon: 'devices'
  font:
    text: 'Roboto'
    code: 'Roboto Mono'
  feature:
    tabs: true

# Customization
extra:
  social:
    - type: 'gitlab'
      link: 'https://gitlab.com/gael.grasset'
    - type: 'linkedin'
      link: 'https://linkedin.com/in/gaelgrasset'

# Google Analytics
google_analytics:
  - 'UA-122173178-1'
  - 'auto'

# Extensions
markdown_extensions:
  - admonition
  - codehilite:
      guess_lang: false
  - meta
  - toc:
      permalink: true
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.superfences
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde

# Pages
pages:
  - Introduction: 'index.md'
  - My Home automation setup:
    - Overall Setup: 'setup/overall-setup.md'
    - OpenHab & MQTT: 'setup/openhab.md'
    - ZWave: setup/zwave.md
    - ESP8266 & Homie: setup/esp8266_homie.md
    - Firmware management: setup/firmware_mgt.md
  - Projects:
    - Temp / Humidity sensor: projects/temp_humidity_sensor.md
    - WS2812B Led effects: projects/ws2812b_led.md
    - Arcade buttons: projects/arcade_buttons.md
    - Other projects in the bucket list...: projects/TODO.md
  - About me: about.md